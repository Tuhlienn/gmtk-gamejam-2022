﻿namespace Tiles
{
    public class BaseTile : TileBase
    {
        public override bool CheckLevelInput(string input)
        {
            return input.Equals("B");
        }

        public override void InitializeFromLevelInput(string input) { }
    }
}