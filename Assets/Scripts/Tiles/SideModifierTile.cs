﻿using System.Linq;
using System.Threading.Tasks;
using Player;
using UnityEngine;

namespace Tiles
{
    public class SideModifierTile : TileBase
    {
        [SerializeField] private int modificationValue = 1;

        public override bool CheckLevelInput(string input)
        {
            if (input.Length < 2)
                return false;

            var firstCharacter = input.First();
            if (firstCharacter is not ('-' or '+')) return false;

            var number = input[1..];
            return number.All(char.IsDigit);
        }

        public override void InitializeFromLevelInput(string input)
        {
            int.TryParse(input, out var modifierInput);
            modificationValue = modifierInput;
        }

        protected override void UpdateSprite()
        {
            var index = modificationValue < 0 ? modificationValue + 5 : modificationValue + 4;

            var newMaterial = materials.ElementAtOrDefault(index);
            meshRenderer.material = newMaterial;
        }

        public override Task OnEnter(PlayerMovement player)
        {
            player.CurrentLowerSide.AddNumberValue(modificationValue);
            return Task.CompletedTask;
        }
    }
}