﻿using System.Threading.Tasks;
using Player;
using UnityEngine;

namespace Tiles
{
    public abstract class TileBase : MonoBehaviour
    {
        [SerializeField] protected Renderer meshRenderer;
        [SerializeField] protected Material[] materials;

        private void Start()
        {
            UpdateSprite();
        }

        public abstract bool CheckLevelInput(string input);
        public abstract void InitializeFromLevelInput(string input);
        protected virtual void UpdateSprite() {}
        public virtual Task OnEnter(PlayerMovement player)
        {
            return null;
        }
        public virtual Task OnExit(PlayerMovement player)
        {
            return null;
        }
    }
}