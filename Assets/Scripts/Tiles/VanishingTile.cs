﻿using System.Linq;
using System.Threading.Tasks;
using DG.Tweening;
using Player;
using UnityEngine;

namespace Tiles
{
    public class VanishingTile : TileBase
    {
        private int Health { get; set; } = 10;
        private bool Dead => Health <= 0;

        public override bool CheckLevelInput(string input)
        {
            if (input.Length < 1)
                return false;

            int.TryParse(input, out var healthInput);
            return healthInput > 0;
        }

        public override void InitializeFromLevelInput(string input)
        {
            int.TryParse(input, out var healthInput);
            Health = healthInput;
        }

        protected override void UpdateSprite()
        {
            var newMaterial = materials.ElementAtOrDefault(Health);
            meshRenderer.material = newMaterial;
        }

        public override async Task OnExit(PlayerMovement player)
        {
            if (Dead) await VanishAsync();
        }

        public override async Task OnEnter(PlayerMovement player)
        {
            var damage = player.CurrentNumber;
            if (damage == 0) return;

            if (damage > Health)
            {
                await VanishAsync();
                player.StartFalling();
            }
            Health = Mathf.Max(Health - damage, 0);
            UpdateSprite();
        }

        private async Task VanishAsync()
        {
            await meshRenderer.material.DOFade(0, 0.5f).AsyncWaitForCompletion();

            var colliders = GetComponentsInChildren<Collider>();
            foreach (var col in colliders)
            {
                col.enabled = false;
            }
            Destroy(gameObject);
        }
    }
}