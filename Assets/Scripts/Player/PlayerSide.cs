﻿using System.Linq;
using UnityEngine;

namespace Player
{
    public class PlayerSide : MonoBehaviour
    {
        private const int MinValue = 1;
        private const int MaxValue = 6;

        [SerializeField] private int initialNumber;
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private Sprite[] sprites;

        public int CurrentNumber { get; private set; }

        private void Start()
        {
            CurrentNumber = initialNumber;
            UpdateSprite();
        }

        public void ResetNumberValue()
        {
            CurrentNumber = initialNumber;
            UpdateSprite();
        }

        public void AddNumberValue(int addedNumber)
        {
            CurrentNumber = Mathf.Clamp(CurrentNumber + addedNumber, MinValue, MaxValue);
            UpdateSprite();
        }

        private void UpdateSprite()
        {
            var newSprite = sprites.ElementAt(CurrentNumber - 1);
            SetSprite(newSprite);
        }

        private void SetSprite(Sprite newSprite)
        {
            if (newSprite == null)
            {
                Debug.LogWarning("Sprite for playerSide was empty");
                return;
            }

            spriteRenderer.sprite = newSprite;
        }
    }
}