using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using Bootstrapping;
using DG.Tweening;
using FMODUnity;
using Levels;
using Tiles;
using UnityEngine;

namespace Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private StudioEventEmitter soundEmitter;
        [SerializeField] private PlayerSide[] sides;
        [SerializeField] private float rotationSpeed = 300;
        [SerializeField] private float fallSpeed = 3;

        public PlayerSide CurrentLowerSide { get; private set; }
        public int CurrentNumber => CurrentLowerSide ? CurrentLowerSide.CurrentNumber : 0;
        public bool CanMove => !_isMoving && !_isFalling;

        private bool _isMoving;
        private bool _isFalling;

        private TileBase _currentTile;
        private readonly Collider[] _currentColliders = new Collider[10];
        private LevelManager _levelManager;
        private LevelSoundManager _levelSoundManager;

        private int _movementSteps;

        public int MovementSteps
        {
            get => _movementSteps;
            private set
            {
                _movementSteps = value;
                if (_levelSoundManager) _levelSoundManager.UpdateIntensity(_movementSteps);
            }
        }

        private void Start()
        {
            _levelManager = ServiceLocator.Instance.Get<LevelManager>();
            _levelSoundManager = ServiceLocator.Instance.Get<LevelSoundManager>();
        }

        public void Roll(Vector3 direction)
        {
            StartCoroutine(RollCoroutine(direction));
        }

        public void StartFalling()
        {
            _isFalling = true;
            _levelSoundManager.SetLevelFailed();
            StartCoroutine(ReloadLevel());
        }

        private void OnTriggerStay(Collider other)
        {
            if (_isMoving) return;

            if (!other.CompareTag("Finish")) return;

            if ((other.transform.position - transform.position).magnitude > 0.1) return;

            PlayGoalAnimation();
        }

        private void PlayGoalAnimation()
        {
            _isMoving = true;
            _levelSoundManager.PlaySucceeded();

            var animationSequence = DOTween.Sequence();
            animationSequence.Append(transform.DOScale(Vector3.zero, 1f).SetEase(Ease.InBack));
            animationSequence.Join(transform.DOMoveY(2, 1f).SetEase(Ease.InOutCubic));
            animationSequence.OnComplete(OnFinishGoalAnimation);
            animationSequence.Play();
        }

        private void OnFinishGoalAnimation()
        {
            _levelManager.GoToNextLevel();
        }

        private IEnumerator RollCoroutine(Vector3 direction)
        {
            if (!CanMove)
                yield break;

            _isMoving = true;

            soundEmitter.Play();
            UpdateOldTile();

            var remainingAngle = 90f;
            var rotationCenter = transform.position + direction / 2 + Vector3.down / 2;
            var rotationAxis = Vector3.Cross(Vector3.up, direction);

            while (remainingAngle > 0)
            {
                var rotationAngle = Mathf.Min(rotationSpeed * Time.deltaTime, remainingAngle);
                transform.RotateAround(rotationCenter, rotationAxis, rotationAngle);
                remainingAngle -= rotationAngle;
                yield return null;
            }

            var updateTask = UpdateNewTile();
            yield return new WaitUntil(() => updateTask.IsCompleted);

            MovementSteps++;
            _isMoving = false;
        }

        private void UpdateOldTile()
        {
            if (_currentTile) _currentTile.OnExit(this);
        }

        private async Task UpdateNewTile()
        {
            _currentTile = GetCurrentTile();
            if (!_currentTile)
            {
                StartFalling();
                return;
            }

            CurrentLowerSide = GetCurrentLowerSide();
            await _currentTile.OnEnter(this);
        }

        private TileBase GetCurrentTile()
        {
            Physics.OverlapBoxNonAlloc(transform.position, Vector3.one / 3, _currentColliders);

            return _currentColliders
                .Where(col => col)
                .SelectMany(col => col.GetComponentsInParent<TileBase>())
                .FirstOrDefault(fieldTile => fieldTile);
        }

        private PlayerSide GetCurrentLowerSide()
        {
            PlayerSide result = null;
            var minYPosition = float.MaxValue;

            foreach (var side in sides)
            {
                if (side.transform.position.y >= minYPosition)
                    continue;

                minYPosition = side.transform.position.y;
                result = side;
            }

            return result;
        }

        private IEnumerator ReloadLevel()
        {
            var remainingMovement = 20f;
            while (remainingMovement > 0)
            {
                var movement = fallSpeed * Time.deltaTime;
                transform.Translate(Vector3.down * movement, Space.World);
                remainingMovement -= movement;
                yield return null;
            }

            _levelManager.ReloadCurrentLevel();
        }

        public void Reset()
        {
            _isFalling = false;
            _isMoving = false;
            MovementSteps = 0;
            transform.localScale = Vector3.one;

            foreach(var side in sides) side.ResetNumberValue();
        }
    }
}