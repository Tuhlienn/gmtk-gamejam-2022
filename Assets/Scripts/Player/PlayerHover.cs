﻿using UnityEngine;

namespace Player
{
    public class PlayerHover : MonoBehaviour
    {
        [SerializeField] private Renderer[] renderers;
        [SerializeField] private float hoverAlphaValue = 0.4f;

        private bool _isHovering;

        private void OnMouseEnter()
        {
            if (_isHovering) return;

            _isHovering = true;
            SetAlphaValue(hoverAlphaValue);
        }

        private void OnMouseExit()
        {
            if (!_isHovering) return;

            _isHovering = false;
            SetAlphaValue(1);
        }

        private void SetAlphaValue(float alpha)
        {
            foreach (var r in renderers)
            {
                var material = r.material;
                var newColor = material.color;
                newColor.a = alpha;
                material.color = newColor;
            }
        }
    }
}