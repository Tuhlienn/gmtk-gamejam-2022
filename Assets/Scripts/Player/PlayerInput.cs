﻿using CameraLogic;
using Lean.Touch;
using UnityEngine;
using Util;

namespace Player
{
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] private PlayerMovement player;
        [SerializeField] private CameraMovement cameraMovement;

        private void OnEnable()
        {
            LeanTouch.OnFingerSwipe += HandleSwipe;
        }

        private void OnDisable()
        {
            LeanTouch.OnFingerSwipe -= HandleSwipe;
        }

        private void HandleSwipe(LeanFinger leanFinger)
        {
            if (!FingerStartedOnPlayer(leanFinger)) return;

            var swipeScreenDirection = (leanFinger.LastScreenPosition - leanFinger.StartScreenPosition).normalized;
            var cameraRotation = cameraMovement.YRotation;

            var direction = new Vector3(swipeScreenDirection.x, 0, swipeScreenDirection.y);
            direction = direction.RotateAngleAxis(cameraRotation, Vector3.up);
            direction = direction.RoundToNextUnitVector();
            player.Roll(direction);
        }

        public static bool FingerStartedOnPlayer(LeanFinger leanFinger)
        {
            var layerMask = LayerMask.GetMask("Player");
            return Physics.Raycast(leanFinger.GetStartRay(), 100, layerMask);
        }
    }
}