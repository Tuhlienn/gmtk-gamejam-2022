﻿using System;
using UnityEditor;
using UnityEngine;

namespace Core
{
    public class Entity : MonoBehaviour
    {
        [MonoBehaviourId]
        [SerializeField] private string id;
        public string Id => id;
    }

    public class MonoBehaviourIdAttribute : PropertyAttribute
    { }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(MonoBehaviourIdAttribute))]
    public class MonoBehaviourIdDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (string.IsNullOrWhiteSpace(property.stringValue))
            {
                property.stringValue = Guid.NewGuid().ToString();
            }

            EditorGUI.PropertyField(position, property, label, true);
        }
    }
#endif
}