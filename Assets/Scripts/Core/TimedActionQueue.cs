﻿using System;
using UnityEngine;
using Util.PriorityQueue;

namespace Core
{
    public class TimedActionQueue : MonoBehaviour
    {
        private readonly PriorityQueue<float, Action> _timedActions = new(float.MaxValue);

        private void Update()
        {
            var nextEntryTime = GetNextEntryTime();
            while (nextEntryTime < Time.time)
            {
                _timedActions.Pop()?.Item2?.Invoke();
                nextEntryTime = GetNextEntryTime();
            }
        }

        public void InvokeAfterSeconds(Action action, float performTimeOffset)
        {
            _timedActions.Insert(Time.time + performTimeOffset, action);
        }

        public void InvokeAtTime(Action action, float performTime)
        {
            _timedActions.Insert(performTime, action);
        }

        public void InvokeSoon(Action action)
        {
            _timedActions.Insert(Time.time + 0.1f, action);
        }

        private float GetNextEntryTime()
        {
            var nextEntry = _timedActions.Peek();
            return nextEntry?.Item1 ?? float.MaxValue;
        }
    }
}