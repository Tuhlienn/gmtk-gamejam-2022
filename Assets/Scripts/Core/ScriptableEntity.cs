﻿using System;
using UnityEditor;
using UnityEngine;

namespace Core
{
    public class ScriptableEntity : ScriptableObject
    {
        [ScriptableObjectId]
        [SerializeField] private string id;
        public string Id => id;
    }

    public class ScriptableObjectIdAttribute : PropertyAttribute
    { }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ScriptableObjectIdAttribute))]
    public class ScriptableObjectIdDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (string.IsNullOrWhiteSpace(property.stringValue))
            {
                property.stringValue = Guid.NewGuid().ToString();
            }

            EditorGUI.PropertyField(position, property, label, true);
        }
    }
#endif
}