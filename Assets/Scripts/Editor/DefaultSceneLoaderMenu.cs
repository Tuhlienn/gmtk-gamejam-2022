﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace Editor
{
    public static class DefaultSceneLoaderMenu
    {
        private const string PrefKey = "PLAY_MODE_SCENE_SELECTION";
        private const string Path = "Custom/Play Scene/";
        private const string MenuDefault = "Default (Open Scene)";
        private const string MenuEmulate = "Emulate Build Behaviour";

        private static string SelectedPlayModeScene
        {
            get => EditorPrefs.GetString(PrefKey, MenuDefault);
            set => EditorPrefs.SetString(PrefKey, value);
        }

        [InitializeOnLoadMethod]
        private static void InitializeSelection()
        {
            switch (SelectedPlayModeScene)
            {
                case MenuDefault:
                    SelectDefault();
                    break;
                case MenuEmulate:
                    SelectEmulateBuild();
                    break;
            }
        }

        private static SceneAsset GetSceneAssetByIndex(int buildIndex)
        {
            var path = SceneUtility.GetScenePathByBuildIndex(buildIndex);
            return AssetDatabase.LoadAssetAtPath<SceneAsset>(path);
        }

        private static void CheckItemIfSelected(string value)
        {
            var isSelected = SelectedPlayModeScene == value;
            Menu.SetChecked(Path + value, isSelected);
        }

        [MenuItem(Path + MenuDefault)]
        public static void SelectDefault()
        {
            EditorSceneManager.playModeStartScene = null;
            SelectedPlayModeScene = MenuDefault;
        }

        [MenuItem(Path + MenuDefault, true)]
        public static bool SelectDefaultValidator()
        {
            CheckItemIfSelected(MenuDefault);
            return true;
        }

        [MenuItem(Path + MenuEmulate)]
        public static void SelectEmulateBuild()
        {
            EditorSceneManager.playModeStartScene = GetSceneAssetByIndex(0);
            SelectedPlayModeScene = MenuEmulate;
        }

        [MenuItem(Path + MenuEmulate, true)]
        public static bool SelectEmulateBuildValidator()
        {
            CheckItemIfSelected(MenuEmulate);
            return true;
        }
    }
}