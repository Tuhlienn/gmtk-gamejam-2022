﻿using UnityEditor;
using UnityEditor.SceneManagement;

namespace Editor
{
    public static class SceneLoaderMenu
    {
        [MenuItem("Custom/Scenes/Menu #1")]
        private static void OpenInitScene()
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/Scenes/InitScene.unity", OpenSceneMode.Single);
            EditorSceneManager.OpenScene("Assets/Scenes/MenuScene.unity", OpenSceneMode.Additive);
        }

        [MenuItem("Custom/Scenes/Game #2")]
        private static void OpenMainScene()
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/Scenes/InitScene.unity", OpenSceneMode.Single);
            EditorSceneManager.OpenScene("Assets/Scenes/GameScene.unity", OpenSceneMode.Additive);
        }
    }
}