﻿using Lean.Touch;
using Player;
using UnityEngine;

namespace CameraLogic
{
    public class CameraMovement : MonoBehaviour
    {
        [SerializeField] private Transform player;
        [SerializeField] private float rotationSensitivity = 1;
        [SerializeField] private float damping = 10;
        [SerializeField] private bool invertRotation;

        public float YRotation => transform.rotation.eulerAngles.y;

        private float _anchorRotation;

        private void OnEnable()
        {
            _anchorRotation = transform.eulerAngles.y;

            LeanTouch.OnFingerUpdate += HandleDrag;
        }

        private void OnDisable()
        {
            LeanTouch.OnFingerUpdate -= HandleDrag;
        }

        private void Update()
        {
            MoveCameraTowards(player.position);
            RotateAnchorBy(_anchorRotation);
        }

        private void HandleDrag(LeanFinger leanFinger)
        {
            if (leanFinger.Pressure == 0 || PlayerInput.FingerStartedOnPlayer(leanFinger))
            {
                _anchorRotation = 0;
                return;
            }

            var fingerHorizontalOffset = leanFinger.ScreenDelta.x;
            var rotationDirection = invertRotation ? -1 : 1;
            _anchorRotation = rotationDirection * fingerHorizontalOffset * rotationSensitivity;
        }

        private void RotateAnchorBy(float anchorRotation)
        {
            transform.Rotate(Vector3.up, anchorRotation * Time.deltaTime);
        }

        private void MoveCameraTowards(Vector3 playerPosition)
        {
            var cameraTransform = transform;
            var newPosition = new Vector3(playerPosition.x, 1, playerPosition.z);
            cameraTransform.position = Vector3.Lerp(cameraTransform.position, newPosition, Time.deltaTime * damping);
        }
    }
}