﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Util.Singletons;

namespace Bootstrapping
{
    public class ServiceLocator : PersistentSingleton<ServiceLocator>
    {
        private readonly Dictionary<Type, object> _instances = new();
        private readonly Dictionary<Type, Func<ServiceLocator, object>> _factories = new();

        public ServiceLocator Register<T>(T instance)
        {
            var type = typeof(T);

            if (IsNotRegistered(type))
                _instances[type] = instance;

            return this;
        }

        public ServiceLocator RegisterWithOverride<T>(T instance)
        {
            var type = typeof(T);
            _instances[type] = instance;
            return this;
        }

        public ServiceLocator RegisterPerRequest<T>(Func<ServiceLocator, T> factoryMethod) where T : class
        {
            var type = typeof(T);

            if (IsNotRegistered(type))
                _factories[type] = factoryMethod;

            return this;
        }

        public ServiceLocator RegisterPerRequestWithOverride<T>(Func<ServiceLocator, T> factoryMethod) where T : class
        {
            var type = typeof(T);
            _factories[type] = factoryMethod;
            return this;
        }

        public T Get<T>() where T : class
        {
            if (TryGetInstance(out T result) || TryGetInstanceFromFactory(out result))
                return result;

            Debug.LogError($"Could not find instance for parameter of type {typeof(T).FullName}.", this);
            return result;
        }

        private bool TryGetInstance<T>(out T instance) where T : class
        {
            instance = null;

            if (false == _instances.TryGetValue(typeof(T), out var obj))
                return false;

            instance = (T) obj;
            return true;
        }

        private bool TryGetInstanceFromFactory<T>(out T instance) where T : class
        {
            instance = null;

            if (false == _factories.TryGetValue(typeof(T), out var objFactory))
                return false;

            instance = (T) objFactory.Invoke(this);
            return true;
        }

        private bool IsNotRegistered(Type type)
        {
            if (!_instances.ContainsKey(type) && !_factories.ContainsKey(type))
                return true;

            Debug.LogError($"Type {type.FullName} has already been registered.", this);
            return false;
        }
    }
}