﻿using System;
using Core;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Bootstrapping
{
    public class ApplicationBootstrapper : MonoBehaviour
    {
        [SerializeField] private Scenes startScene;
        [SerializeField] private Camera mainCamera;
        [SerializeField] private LevelLoader levelLoader;
        [SerializeField] private TimedActionQueue timedActionQueue;

        private void Awake()
        {
            Bootstrap().Forget();
        }

        private async UniTask Bootstrap()
        {
            levelLoader.Initialize();

            ServiceLocator.Instance
                .Register(mainCamera)
                .Register(levelLoader)
                .Register(timedActionQueue);

            await SceneManager.LoadSceneAsync(startScene.ToString(), LoadSceneMode.Additive);
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(startScene.ToString()));
        }
    }

    [Serializable]
    public enum Scenes
    {
        InitScene,
        MenuScene,
        GameScene
    }
}