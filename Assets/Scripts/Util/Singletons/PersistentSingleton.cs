using UnityEngine;

namespace Util.Singletons
{
    public class PersistentSingleton<T> : MonoBehaviour	where T : Component
    {
        private static T _instance;
        public static T Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;

                _instance = FindObjectOfType<T> ();
                if (_instance != null)
                    return _instance;

                var obj = new GameObject ();
                _instance = obj.AddComponent<T> ();
                return _instance;
            }
        }

        public bool Enabled { get; private set; }

        protected virtual void Awake ()
        {
            if (!Application.isPlaying)
            {
                return;
            }

            if(_instance == null)
            {
                _instance = this as T;
                DontDestroyOnLoad (transform.gameObject);
                Enabled = true;
            }
            else
            {
                if(this != _instance)
                {
                    Destroy(gameObject);
                }
            }
        }
    }
}
