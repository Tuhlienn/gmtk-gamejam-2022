﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Util
{
    public static class ReflectionUtil
    {
        public static Dictionary<string, T> GetPropertyValuesByType<T>(this object self)
        {
            var result = new Dictionary<string, T>();

            if (self == null)
                return result;

            var members = self.GetType()
                .GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(member => member.MemberType is MemberTypes.Field or MemberTypes.Property);

            var filteredMembers = members.Where(member => GetType(member) == typeof(T));

            return filteredMembers
                .ToDictionary(
                    member => member.Name,
                    member => member.GetValue<T>(self));
        }

        private static Type GetType(MemberInfo member)
        {
            return member.MemberType switch
            {
                MemberTypes.Field => ((FieldInfo)member).FieldType,
                MemberTypes.Property => ((PropertyInfo)member).PropertyType,
                MemberTypes.Event => ((EventInfo)member).EventHandlerType,
                _ => null
            };
        }

        private static T GetValue<T>(this MemberInfo member, object from)
        {
            return member.MemberType switch
            {
                MemberTypes.Field => (T)((FieldInfo)member).GetValue(from),
                MemberTypes.Property => (T)((PropertyInfo)member).GetValue(from, null),
                _ => throw new ArgumentOutOfRangeException(
                    nameof(member),
                    "Property must be of type FieldInfo or PropertyInfo")
            };
        }
    }
}