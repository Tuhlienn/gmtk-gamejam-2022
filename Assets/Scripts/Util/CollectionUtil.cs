﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Util
{
    public static class CollectionUtil
    {
        public static T RandomItem<T>(this IList<T> list)
        {
            if (list.Count == 0) throw new System.IndexOutOfRangeException("Cannot select a random item from an empty list");
            return list[Random.Range(0, list.Count)];
        }

        public static IList<T> RandomItems<T>(this IList<T> list, int requestedAmount)
        {
            var result = new List<T>();
            for (var i = 0; i < requestedAmount; i++)
            {
                var availableItems = list.Where(entry => !result.Contains(entry)).ToList();
                if (!availableItems.Any())
                    return result;

                var selectedItem = availableItems.RandomItem();
                result.Add(selectedItem);
            }

            return result;
        }
    }
}