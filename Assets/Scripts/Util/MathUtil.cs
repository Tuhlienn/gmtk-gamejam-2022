using System;
using UnityEngine;

namespace Util
{
    public static class MathUtil
    {
        /// <summary>
        /// A small number that can be used for floating point arithmetic / approximations
        /// </summary>
        public const float SmallNumber = 1e-8f;

        public static double Deg2Rad(double deg)
        {
            return Math.PI / 180.0 * deg;
        }

        public static double Rad2Deg(double rad)
        {
            return 180.0 / Math.PI * rad;
        }

        /// <summary>
        /// returns the vector3 with only Z and Y components i.e. x is zeroed
        /// </summary>
        public static Vector3 WithZeroX(this Vector3 vector)
        {
            vector.x = 0;
            return vector;
        }

        /// <summary>
        /// returns the vector3 with only X and Z components i.e. y is zeroed
        /// </summary>
        public static Vector3 WithZeroY(this Vector3 vector)
        {
            vector.y = 0;
            return vector;
        }

        /// <summary>
        /// returns the vector3 with only X and Y components i.e. z is zeroed
        /// </summary>
        public static Vector3 WithZeroZ(this Vector3 vector)
        {
            vector.z = 0;
            return vector;
        }

        public static Vector3 WithZeroZ(this Vector2 vector)
        {
            return vector.WithZ(0);
        }

        public static Vector3 WithZ(this Vector2 vector, float z)
        {
            return new Vector3(vector.x, vector.y, z);
        }

        public static Vector3Int WithZeroZ(this Vector2Int vector)
        {
            return vector.WithZ(0);
        }

        public static Vector3Int WithZ(this Vector2Int vector, int z)
        {
            return new Vector3Int(vector.x, vector.y, z);
        }

        public static Vector2Int ToNormalizedGridDirection(this Vector2 vector)
        {
            return Mathf.Abs(vector.x) > Mathf.Abs(vector.y)
                ? new Vector2Int((int)Mathf.Sign(vector.x), 0)
                : new Vector2Int(0, (int)Mathf.Sign(vector.y));
        }

        public static Vector2 Rotate(this Vector2 vector, float angleDeg)
        {
            var sin = Mathf.Sin(angleDeg * Mathf.Deg2Rad);
            var cos = Mathf.Cos(angleDeg * Mathf.Deg2Rad);

            var tx = vector.x;
            var ty = vector.y;
            vector.x = (cos * tx) - (sin * ty);
            vector.y = (sin * tx) + (cos * ty);
            return vector;
        }

        /// <summary>
        /// Projects the given vector onto the plane specified by normal.
        /// This preserves the vectors XZ direction
        /// </summary>
        /// <param name="vector">Vector to project onto the plane</param>
        /// <param name="normal">normal of the plane to project onto</param>
        /// <returns>vector orthographically projected onto the plane</returns>
        public static Vector3 ProjectOrthogonalOnPlanePreserveXZDir(Vector3 vector, Vector3 normal)
        {
            Debug.Assert(Mathf.Approximately(normal.magnitude, 1));
            var projected = vector;
            projected.y = -(vector.x * normal.x + vector.z * normal.z) / normal.y;
            projected = projected.normalized * vector.magnitude;
            return projected;
        }

        public static float DistanceSquared(Vector2 a, Vector2 b)
        {
            return (a - b).sqrMagnitude;
        }

        /// <summary>
        /// Rotates a vector by a given angle around a specified axis
        /// </summary>
        /// <param name="vector">the vector to rotate</param>
        /// <param name="angleDeg">the amount to rotate in degree</param>
        /// <param name="axis">the axis to rotate around</param>
        /// <returns></returns>
        public static Vector3 RotateAngleAxis(this Vector3 vector, float angleDeg, Vector3 axis)
        {
            var rad = Mathf.Deg2Rad * angleDeg;
            var s = Mathf.Sin(rad);
            var c = Mathf.Cos(rad);

            var xx = axis.x * axis.x;
            var yy = axis.y * axis.y;
            var zz = axis.z * axis.z;

            var xy = axis.x * axis.y;
            var yz = axis.y * axis.z;
            var zx = axis.z * axis.x;

            var xs = axis.x * s;
            var ys = axis.y * s;
            var zs = axis.z * s;

            var omc	= 1f - c;

            var x = vector.x;
            var y = vector.y;
            var z = vector.z;

            return new Vector3(
                (omc * xx + c) * x + (omc * xy - zs) * y + (omc * zx + ys) * z,
                (omc * xy + zs) * x + (omc * yy + c) * y + (omc * yz - xs) * z,
                (omc * zx - ys) * x + (omc * yz + xs) * y + (omc * zz + c) * z);
        }

        /// <summary>
        /// Calculates the pointwise product of two vectors.
        /// </summary>
        public static Vector3 PointwiseProduct(this Vector3 a, Vector3 b)
        {
            return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z );
        }

        /// <summary>
        /// Calculates the pointwise product of two vectors.
        /// </summary>
        public static Vector2 PointwiseProduct(this Vector2 a, Vector2 b)
        {
            return new Vector2(a.x * b.x, a.y * b.y);
        }

        public static Vector3 RoundToNextUnitVector(this Vector3 vector)
        {
            var absX = Mathf.Abs(vector.x);
            var absY = Mathf.Abs(vector.y);
            var absZ = Mathf.Abs(vector.z);

            if (absX > absY && absX > absZ)
            {
                return new Vector3(Mathf.Sign(vector.x), 0, 0);
            }

            return absY > absZ
                ? new Vector3(0, Mathf.Sign(vector.y), 0)
                : new Vector3(0, 0, Mathf.Sign(vector.z));
        }
    }
}