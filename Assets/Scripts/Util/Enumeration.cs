﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Core
{
    public abstract class Enumeration : IComparable
    {
        public int Id { get; }
        public string DisplayName { get; }

        protected Enumeration(int id, string displayName) => (Id, DisplayName) = (id, displayName);

        public override string ToString() => DisplayName;

        public static IEnumerable<T> GetAll<T>() where T : Enumeration
        {
            return typeof(T)
                .GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly)
                .Select(f => f.GetValue(null))
                .Cast<T>();
        }

        public override bool Equals(object obj)
        {
            if (obj is not Enumeration otherValue)
            {
                return false;
            }

            if (GetType() != obj.GetType())
                return false;

            return Id.Equals(otherValue.Id);
        }

        public static T FromId<T>(int id) where T : Enumeration, new()
        {
            var matchingItem = Parse<T, int>(id, "id", item => item.Id == id);
            return matchingItem;
        }

        public static T FromDisplayName<T>(string displayName) where T : Enumeration, new()
        {
            var matchingItem = Parse<T, string>(displayName, "display name", item => item.DisplayName == displayName);
            return matchingItem;
        }

        private static T Parse<T, K>(K value, string description, Func<T, bool> predicate) where T : Enumeration, new()
        {
            var matchingItem = GetAll<T>().FirstOrDefault(predicate);

            if (matchingItem != null)
                return matchingItem;

            var message = $"'{value}' is not a valid {description} in {typeof(T)}";
            throw new ApplicationException(message);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public int CompareTo(object other) => Id.CompareTo(((Enumeration)other).Id);
    }
}