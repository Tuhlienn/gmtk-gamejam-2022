﻿using System;

namespace Util.PriorityQueue
{
    public interface IPriorityQueue<TKey, T> where TKey : IComparable<TKey>
    {
        /// <summary>
        /// Inserts and item with a priority
        /// </summary>
        /// <param name="item"></param>
        /// <param name="priority"></param>
        void Insert(TKey priority, T item);

        /// <returns>The element with the highest priority</returns>
        Tuple<TKey, T> Peek();

        /// <summary>
        /// Deletes the element with the highest priority
        /// </summary>
        /// <returns>The deleted element</returns>
        Tuple<TKey, T> Pop();
    }
}