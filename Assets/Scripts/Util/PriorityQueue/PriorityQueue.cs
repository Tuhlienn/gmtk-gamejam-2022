﻿using System;

namespace Util.PriorityQueue
{
    public class PriorityQueue<TPriority, TElement> : IPriorityQueue<TPriority, TElement> where TPriority : IComparable<TPriority>
    {
        private readonly FibonacciHeap<TElement, TPriority> heap;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="minPriority">Minimum value of the priority - to be used for comparing.</param>
        public PriorityQueue(TPriority minPriority)
        {
            heap = new FibonacciHeap<TElement, TPriority>(minPriority);
        }

        public void Insert(TPriority priority, TElement item)
        {
            heap.Insert(new FibonacciHeapNode<TElement, TPriority>(item, priority));
        }

        public Tuple<TPriority, TElement> Peek()
        {
            var node = heap.Min();
            return node == null
                ? null
                : new Tuple<TPriority, TElement>(node.Key, node.Data);
        }

        public Tuple<TPriority, TElement> Pop()
        {
            var node = heap.RemoveMin();
            return node == null
                ? null
                : new Tuple<TPriority, TElement>(node.Key, node.Data);
        }
    }
}