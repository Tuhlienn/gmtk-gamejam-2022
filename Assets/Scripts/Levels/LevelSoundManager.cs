﻿using System.Collections;
using Bootstrapping;
using FMOD.Studio;
using FMODUnity;
using UnityEngine;
using UnityEngine.UI;

namespace Levels
{
    public class LevelSoundManager : MonoBehaviour
    {
        private const string IntensityParameter = "intensity";
        private const string SucceededParameter = "playerSucceeded";
        private const string FailedParameter = "playerFailed";

        [SerializeField] private StudioEventEmitter musicEventEmitter;
        [SerializeField] private StudioEventEmitter succeededEmitter;
        [SerializeField] private StudioEventEmitter failedEmitter;
        [SerializeField] private Slider volumeSlider;

        private LevelManager _levelManager;
        private LevelManager LevelManager => _levelManager ??= ServiceLocator.Instance.Get<LevelManager>();

        private Bus _masterBus;

        private void Awake()
        {
            ServiceLocator.Instance.Register(this);
        }

        private void Start()
        {
            musicEventEmitter.Play();

            _masterBus = RuntimeManager.GetBus("bus:/");
        }

        public void UpdateVolume(float value)
        {
            _masterBus.setVolume(value);
        }

        public void UpdateIntensity(int movementSteps)
        {
            var ratio = movementSteps / (float)LevelManager.CurrentLevelDefinition.expectedMovementSteps;
            var mappedRatio = Mathf.Sqrt(ratio);
            musicEventEmitter.SetParameter(IntensityParameter, mappedRatio * 100, true);
        }

        public void PlaySucceeded()
        {
            musicEventEmitter.SetParameter(SucceededParameter, 1, true);
            StartCoroutine(ResetParameterAfterTime(SucceededParameter, 5));
            if (succeededEmitter) succeededEmitter.Play();
        }

        public void SetLevelFailed()
        {
            musicEventEmitter.SetParameter(FailedParameter, 1, true);
            StartCoroutine(ResetParameterAfterTime(FailedParameter, 2.2f));
            if (failedEmitter) failedEmitter.Play();
        }

        private IEnumerator ResetParameterAfterTime(string parameter, float seconds)
        {
            yield return new WaitForSeconds(seconds);
            musicEventEmitter.SetParameter(parameter, 0, true);
        }
    }
}