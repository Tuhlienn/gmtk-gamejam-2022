using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] private string levelFolderPath = "Levels";

    private readonly List<LevelDefinition> _levelDefinitions = new();

    public int LevelCount => _levelDefinitions.Count;

    public void Initialize()
    {
        var levelTextFiles = Resources.LoadAll<TextAsset>(levelFolderPath);
        foreach (var levelFile in levelTextFiles)
        {
            var content = levelFile.ToString();
            var levelDefinition = JsonConvert.DeserializeObject<LevelDefinition>(content);
            _levelDefinitions.Add(levelDefinition);
        }
    }

    public LevelDefinition GetLevel(int index)
    {
        return _levelDefinitions.ElementAtOrDefault(index);
    }
}

[Serializable]
public class LevelDefinition
{
    public int[] startPosition;
    public int[] startRotation;
    public int[] endPosition;
    public string[][] tiles;
    public int expectedMovementSteps;

    public Vector2 GetStartPosition()
    {
        return new Vector2(startPosition[0], startPosition[1]);
    }

    public Vector3 GetStartRotation()
    {
        return new Vector3(startRotation[0], startRotation[1], startRotation[2]);
    }

    public Vector2 GetEndPosition()
    {
        return new Vector2(endPosition[0], endPosition[1]);
    }
}
