﻿using System.Collections.Generic;
using System.Linq;
using Bootstrapping;
using Player;
using Tiles;
using UnityEngine;

namespace Levels
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] private PlayerMovement player;
        [SerializeField] private Transform goalTransform;

        [SerializeField] private TileBase[] tilePrefabs;

        public LevelDefinition CurrentLevelDefinition => _levelLoader.GetLevel(_currentLevelIndex);

        private LevelLoader _levelLoader;
        private int _currentLevelIndex;
        private readonly List<GameObject> _currentLevelObjects = new();

        private void Awake()
        {
            ServiceLocator.Instance.Register(this);
        }

        private void Start()
        {
            _levelLoader = ServiceLocator.Instance.Get<LevelLoader>();
            ReloadCurrentLevel();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                _currentLevelIndex = 0;
                ReloadCurrentLevel();
            }

            for (int i = 0; i < _levelLoader.LevelCount; i++)
            {
                if (!Input.GetKeyDown($"{i}"))
                    continue;

                _currentLevelIndex = i;
                ReloadCurrentLevel();
                break;
            }
        }

        public void ReloadCurrentLevel()
        {
            LoadByIndex(_currentLevelIndex);
        }

        public void LoadByIndex(int index)
        {
            var levelDefinition = _levelLoader.GetLevel(index);
            if (levelDefinition == null) return;

            _currentLevelIndex = index;

            ClearLevelObjects();

            SetRotation(player.transform, levelDefinition.GetStartRotation());
            SetPosition(player.transform, levelDefinition.GetStartPosition());
            player.Reset();
            SetPosition(goalTransform, levelDefinition.GetEndPosition());
            CreateLevelTiles(levelDefinition.tiles);
        }

        private static void SetRotation(Transform target, Vector3 targetRotation)
        {
            target.rotation = Quaternion.Euler(targetRotation);
        }

        private static void SetPosition(Transform target, Vector2 targetPosition)
        {
            var position = new Vector3(
                targetPosition.x,
                1,
                targetPosition.y);

            target.position = position;
        }

        private void ClearLevelObjects()
        {
            foreach (var levelObject in _currentLevelObjects)
            {
                Destroy(levelObject);
            }
            _currentLevelObjects.Clear();
        }

        private void CreateLevelTiles(string[][] tileMap)
        {
            for (var z = 0; z < tileMap.Length; z++)
            {
                var row = tileMap[z];
                for (var x = 0; x < row.Length; x++)
                {
                    var tileInput = row[x];
                    var tilePrefab = GetTilePrefab(tileInput);
                    if (tilePrefab == null)
                        continue;

                    var position = new Vector3(row.Length - x - 1, 0, tileMap.Length - z - 1);
                    var newField = Instantiate(tilePrefab, position, Quaternion.identity, transform);
                    newField.InitializeFromLevelInput(tileInput);
                    _currentLevelObjects.Add(newField.gameObject);
                }
            }
        }

        private TileBase GetTilePrefab(string tileInput)
        {
            if (tileInput.Equals("x")) return null;

            return tilePrefabs.FirstOrDefault(tile => tile.CheckLevelInput(tileInput));
        }

        public void GoToNextLevel()
        {
            _currentLevelIndex++;
            ReloadCurrentLevel();
        }
    }
}